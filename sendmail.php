<?php
$name = $_POST['name'];
$email = $_POST['email'];
$subject = $_POST['subject'];
$message = $_POST['message'];

if (!$name || !$email || !$subject || !$message) {
    echo "<div class='show' class='text-center'>Please complete all form fields.</div>";
} else {
    $to = "phpsitescripts@outlook.com";
    $subject = $subject . " [From Sabrina Portfolio Site]";
    $headers = "From: " . $name . " <" . $email . ">\n";
    $headers .= "Reply-To: <" . $email . ">\n";
    $headers .= "X-Sender: <" . $email . ">\n";
    $headers .= "X-Mailer: PHP5\n";
    $headers .= "X-Priority: 3\n";
    $headers .= "Return-Path: <" . $email . ">\n";

    @mail($to, $subject, wordwrap(stripslashes($message)),$headers, "-f$email");

    echo "<div class='show' class='text-center'>Your Message Was Sent!</div>";
}

exit;