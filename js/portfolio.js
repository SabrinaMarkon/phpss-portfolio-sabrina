$(function() {

    $('.scroll').on('click', function(event) {
        event.preventDefault();
        $('html,body').animate( { scrollTop:$(this.hash).offset().top } , 1000);
        $('.nav').fadeIn("slow");
    });

});

/* Send mail from the contact form */
$('#contactme').click(function(){
    $.post('../sendmail.php',
        {
            name: $('#name').val(),
            email: $('#email').val(),
            subject: $('#subject').val(),
            message: $('#message').val(),
        },
        function(data, status){
            //alert('Data: ' + data + '\nStatus: ' + status + '\n');
            if (status === 'success') {

                $(data).insertAfter( "#contactme" );
                $('.show').delay(3000).fadeOut();

            } else {

                var err = "<div class='show'>Sorry, an error occurred. Please email directly: <a href=\"mailto:phpsitescripts@outlook.com\">phpsitescripts@outlook.com</a></div>";
                $(err).insertAfter( "#contactme" );
                $('.show').delay(2000).fadeOut();

            }
        });
});

/* Hide the mobile menu when the screen size changes. */
$(window).resize(function(){
    $('.navmobile').hide();
})

/* Show or hide the mobile menu when the user clicks the hamburger/ */
function toggleMenu(x) {
    x.classList.toggle("change");
    $('.navmobile').toggle("display");
}

/* Parallax body background */
$(window).scroll(function () {
    $("body").css("background-position","50% " + ($(this).scrollTop() / 2) + "px");
});